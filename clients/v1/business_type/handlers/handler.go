package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	service "gitlab.com/business-type/clients/v1/business_type/services"
	cc "gitlab.com/business-type/helpers/constants"
	model "gitlab.com/business-type/models/v1"
	pb "gitlab.com/business-type/proto/v1/business_type"
)

/*
Handler Business Type
*/
func HandlerBusisnesstype(c *gin.Context) {

	req := &pb.BusinessTypeRequest{
		UserId: "0",
	}

	clientOpen := service.OpenService()
	if response, err := clientOpen.BusinessTypeList(c, req); err == nil {
		if response.GetStatus() == 400 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  500,
				"message": response.GetMessage(),
				"desc":    response.GetDesc(),
				"data":    nil,
			})
			return
		}

		if response.GetStatus() != 200 {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetDesc(),
				"data":    nil,
			})

			return
		} else {
			dataResponse := model.BusinessTypeLists{}
			for _, v := range response.GetData() {
				dataRow := model.BusinessTypeList{
					BusinessTypeCode: int(v.GetId()),
					BusinessType:     v.GetName(),
				}

				dataResponse = append(dataResponse, dataRow)
			}

			c.JSON(http.StatusOK, gin.H{
				"status":  response.GetStatus(),
				"message": response.GetMessage(),
				"desc":    response.GetDesc(),
				"data":    dataResponse,
			})

			return
		}

	} else {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  500,
			"message": cc.ErorrGeneralMessage,
			"desc":    err.Error(),
			"data":    nil,
		})
	}
}
