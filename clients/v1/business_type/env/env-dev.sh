#!/bin/sh

export APP_ENV=DEV
export APP_NAME=PINANG-MIKRO-CLIENT-API-MASTER

export PATH=$PATH:/usr/local/go/bin

export TZ=Asia/Jakarta

export PORT_CLIENT_MASTER=":9010"
export PORT_SERVICE_MASTER=localhost:50051

export API_USER=agri-api
export API_SECRET=AGr1d1g1tal2021

export TOKEN_LIFETIME=60
export REDIS_DB_TOKEN=1

export GCM_SECRETKEY=RayaDigitalBank1
export GCM_NONCE=RayaPiPerTop

export REDIS_HOST=10.100.1.59
export REDIS_PORT=6379
export REDIS_DB=0
export REDIS_PASSWORD=

export DATA_AGRI_PRESCREENING_URL=http://digiagri.briagro.co.id:4343