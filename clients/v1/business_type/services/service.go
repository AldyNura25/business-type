package services

import (
	"context"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	config "gitlab.com/business-type/configs"
	pb "gitlab.com/business-type/proto/v1/business_type"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

/*
Open Service
*/
func OpenService() pb.BusisnessTypesClient {
	portMain := os.Getenv("PORT_CLIENT_MASTER")
	addressMainV1 := os.Getenv("PORT_SERVICE_MASTER")

	maxMsgSize := 1024 * 1024 * 20

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(maxMsgSize),
		grpc.MaxCallSendMsgSize(maxMsgSize),
	))
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))

	connectionService, err := grpc.Dial(
		addressMainV1,
		opts...,
	)

	if err != nil {
		log.Printf("did not connect: %v\n", err)
	}

	log.Println("START OPEN SERVER MASTER IN ", portMain, " HIT : ", time.Now())
	log.Println("RUN SERVICE MASTER IN ", addressMainV1)

	clientOpen := pb.NewBusisnessTypesClient(connectionService)
	return clientOpen
}

/*
Check Token
*/
func TokenAuthentication() gin.HandlerFunc {
	return func(c *gin.Context) {
		authToken := c.Request.Header.Get("Authorization")
		log.Println("Auth Token : ", authToken)

		if authToken == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"code":    403,
				"message": "403 Forbidden",
				"desc":    "Tidak Memliki Akses",
			})

			return
		}

		token := strings.TrimPrefix(authToken, "Bearer ")
		log.Println("Token : ", token)

		ctx := context.Background()
		redisClient := config.InitRedisConnection(config.RedisDBToken())

		checkToken, err := redisClient.Get(ctx, token).Result()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Invalid Token || Token Is Expired",
				"desc":    "Expired",
			})

			return
		}

		log.Println("CHECK TOKEN : ", checkToken)

		if checkToken == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    401,
				"message": "Invalid Token || Token Is Expired",
				"desc":    "Expired",
			})

			return
		}

		c.Next()
	}
}
