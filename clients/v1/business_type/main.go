package main

import (
	"log"
	"net/http"
	"os"
	"time"

	routers "gitlab.com/business-type/clients/v1/business_type/routers"
)

func main() {

	portMain := os.Getenv("PORT_CLIENT_MASTER")

	serverMain := &http.Server{
		Addr:           portMain,
		Handler:        routers.Router(),
		ReadTimeout:    360 * time.Second,
		WriteTimeout:   360 * time.Second,
		MaxHeaderBytes: 1 << 60,
	}

	log.Println("RUN SERVER IN ", portMain, " HIT : ", time.Now())

	serverMain.ListenAndServe()
}
