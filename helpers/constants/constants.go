package helpers

import (
	"net"
	"net/http"
	"time"
)

// type TokenMapping struct {
// 	UserId    string `json:"userId"`
// 	LoginAt   string `json:"loginAt"`
// 	ExpiredAt string `json:"expiredAt"`
// }
var TransportConfig *http.Transport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout:   2 * time.Minute,
		KeepAlive: 30 * time.Second,
	}).Dial, // We use ABSURDLY large keys, and should probably not.	TLSHandshakeTimeout: 60 * time.Second,

}

const (
	AppJWT      = "digital-agri-api"
	IdentityJWT = "d1gitAL@4gR1"
	KeyJWT      = "ZGlnaXRhbC1hZ3JpOjIwMjEwNDIy"

	ErorrGeneralMessage               = "Maaf, Server sedang dalam perbaikan. Silahkan coba beberapa saat lagi."
	ErorrDBDefaultMessageLocal        = "Error Default"
	ErorrDBRecordNotFoundMessageLocal = "Error Record Not Found"
	ErorrDBProvinceNotFoundMessage    = "Provinsi Tidak Ditemukan"
	ErorrDBCityNotFoundMessage        = "Kota / Kabupaten Tidak Ditemukan"
	ErorrDBDistrictNotFoundMessage    = "Kecamatan Tidak Ditemukan"
	ErorrDBVillageNotFoundMessage     = "Kelurahan Tidak Ditemukan"
	ErorrDBPostalCodeNotFoundMessage  = "Kode Pos Tidak Ditemukan"
	DBDataDitemukanMessage            = "Data ditemukan"
	DBDataValidMessageLocal           = "Data Valid"
	DBSaveSuccessMessage              = "Data Berhasil Disimpan"
	DBSaveSuccessMessageLocal         = "Data Tersimpan"
	DBSaveFailedMessage               = "Maaf, Data Gagal Disimpan"
	DBSaveFailedMessageLocal          = "Gagal simpan ke database (row affected)"
	CURLHeaderContentType             = "Content-Type"
	CURLHeaderContentTypeValue        = "application/x-www-form-urlencoded"
	CURLHeaderCacheControl            = "Cache-Control"
	CURLHeaderCacheControlValue       = "no-cache"

	DBDeleteSuccessDataMessage       = "Data Berhasil Dihapus"
	DBDeleteSuccessDataMessageLocal  = "Data Deleted"
	DBDeleteFailedMessage            = "Maaf, Data Gagal Dihapus"
	DBDeleteFailedMessageLocal       = "Gagal hapus dari database (row affected)"
	DBEnableDataMessage              = "Data Berhasil Diaktifkan"
	DBEnableDataMessageLocal         = "Data Enble"
	DBDisableDataMessage             = "Data Berhasil Dinonaktifkan"
	DBDisableDataMessageLocal        = "Data Disable"
	DBEnableDataFailedMessage        = "Data Gagal Diaktifkan"
	DBEnableDataFailedMessageLocal   = "Data Gagal Dienable dari Database (row affected)"
	DBDisableDataFailedMessage       = "Data Gagal Dinonaktifkan"
	DBDisableDataFailedMessageLocal  = "Data Data Gagal Didisable dari Database (row affected)"
	DBAlreadyEnableDataMessage       = "Maaf, Data Sudah Dalam Status Aktif"
	DBAlreadyDisableDataMessage      = "Maaf, Data Sudah Dalam Status Non-aktif"
	DBAlreadyEnableDataMessageLocal  = "Data Gagal Diaktifkan (data already enabled)"
	DBAlreadyDisableDataMessageLocal = "Data Gagal Dinonaktifkan (data already disabled)"
	ErrorTimeout                     = "Maaf, Sesi kamu untuk mengakses telah habis. Kamu dapat mencoba kembali secara berkala!"
	ErrorGeneral                     = "Maaf, Saat ini sedang terjadi perbaikan pada sistem. Kami akan segera kembali!"
)
