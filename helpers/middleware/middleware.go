package middleware

import (
	"log"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	config "gitlab.com/business-type/configs"
	cc "gitlab.com/business-type/helpers/constants"
	model "gitlab.com/business-type/models/v1"
)

func Middleware() *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:      cc.AppJWT,
		Key:        []byte(cc.KeyJWT),
		Timeout:    168 * time.Hour,
		MaxRefresh: 168 * time.Hour,

		IdentityKey: cc.IdentityJWT,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*model.User); ok {
				return jwt.MapClaims{
					cc.IdentityJWT: v.UserName,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &model.User{
				UserName: claims[cc.IdentityJWT].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals model.Login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			userID := loginVals.Username
			password := loginVals.Password

			statusAuth, _ := config.GetAuth(userID, password, "FALSE")

			if statusAuth == 200 {
				return &model.User{
					UserName: userID,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			statusAuth, _ := config.GetAuth(data.(*model.User).UserName, "", "TRUE")

			if statusAuth == 200 {
				return true
			}

			log.Println("---USER NOT MATCH JWT---")

			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			log.Println("---UNAUTORIZED JWT---")

			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},

		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	return authMiddleware
}
