package configs

import (
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/plugin/dbresolver"
)

type V1LoggingCloudPubSub struct {
	Status       string
	Endpoint     string
	UserId       string
	ActionDate   string
	Description  string
	DataRequest  string
	DataResponse string
}

func AppEnv() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	result := os.Getenv("APP_ENV")

	return result, 200
}

func AppName() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	result := os.Getenv("APP_NAME")

	return result, 200
}

func GCMSecretKey() (result string, status int) {
	result = os.Getenv("GCM_SECRETKEY")

	return result, 200
}

func GCMNonce() (result string, status int) {
	result = os.Getenv("GCM_NONCE")

	return result, 200
}

func MediaPath() (result string, status int) {
	url := os.Getenv("MEDIA_PATH")

	// fmt.Println("--MEDIA PATH--")
	// fmt.Println(url)

	return url, 200
}

func MediaBucket() (result string, status int) {
	url := os.Getenv("MEDIA_BUCKET")

	// fmt.Println("--MEDIA BUCKET--")
	// fmt.Println(url)

	return url, 200
}

func GetAuth(username string, password string, justUsername string) (status int, auth map[string]string) {
	userApi, _ := UserAPI()
	secretApi, _ := SecretAPI()

	auth = map[string]string{}
	auth[userApi] = secretApi

	if auth[username] != "" { //check username
		if justUsername == "TRUE" {
			return 200, auth
		}

		if auth[username] == password { //check password
			return 200, auth
		} else {
			return 400, auth
		}
	} else {
		return 400, auth
	}

	return 200, auth
}

func UserAPI() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	result := os.Getenv("API_USER")

	return result, 200
}

func SecretAPI() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	result := os.Getenv("API_SECRET")

	return result, 200
}

func StringToInt(text string) (result int, status int) {
	result, _ = strconv.Atoi(text)

	return result, 200
}

func InitDb() (*gorm.DB, error) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return nil, err
	// }

	dbHost := os.Getenv("PINANGMIKRO_HOST")
	dbUser := os.Getenv("PINANGMIKRO_USER")
	dbPass := os.Getenv("PINANGMIKRO_PASS")
	dbName := os.Getenv("PINANGMIKRO_DB")

	dsnString := []string{"sqlserver://", dbUser, ":", dbPass, "@", dbHost, "?database=", dbName}
	dsn := strings.Join(dsnString, "")
	// fmt.Println("--CONNECTION STRING--")
	// fmt.Println(dsn)
	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	db.Logger.LogMode(logger.Info)

	db.Use(dbresolver.Register(dbresolver.Config{}).SetMaxIdleConns(50).SetMaxOpenConns(100).SetConnMaxLifetime(time.Hour))

	return db, nil
}

func InitRedisConnection(redisDb string) (rdb *redis.Client) {
	redisUrl, _ := RedisUrl()
	redisPassword, _ := RedisPassword()
	redisDB, _ := RedisDB()
	if redisDb != "" {
		redisDB, _ = StringToInt(redisDb)
	}

	rdb = redis.NewClient(&redis.Options{
		Addr:     redisUrl,
		Password: redisPassword, // no password set
		DB:       redisDB,       // use default DB
	})

	return rdb
}

func TokenLifetime() (result int) {
	tokenLifetime := os.Getenv("TOKEN_LIFETIME")
	result, _ = StringToInt(tokenLifetime)

	return result
}

func RedisDBToken() (result string) {
	result = os.Getenv("REDIS_DB_TOKEN")

	return result
}

func RedisUrl() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	stringUrl := []string{host, ":", port}
	result := strings.Join(stringUrl, "")

	return result, 200
}

func RedisPassword() (name string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	result := os.Getenv("REDIS_PASSWORD")

	return result, 200
}

func RedisDB() (name int, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return 0, 400
	// }

	dbRedis := os.Getenv("REDIS_DB")

	result, _ := StringToInt(dbRedis)

	return result, 200
}

func APIDataAgri() (result string, status int) {
	url := os.Getenv("DATA_AGRI_PRESCREENING_URL")

	// fmt.Println("--URL API DATA AGRO PRESCREENING--")
	// fmt.Println(url)

	return url, 200
}

func HostSSO(endpoint string) (result string, status int) {
	// err := godotenv.Load()
	// if err != nil {
	// 	return "", 400
	// }

	host := os.Getenv("SSO_HOST")

	joinString := []string{host, endpoint}
	result = strings.Join(joinString, "")
	// fmt.Println("--DISINI--")
	// fmt.Println(host)
	// fmt.Println(joinString)
	// fmt.Println(result)

	return result, 200
}
