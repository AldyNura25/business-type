package v1

import (
	"errors"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	configs "gitlab.com/business-type/configs"
	cc "gitlab.com/business-type/helpers/constants"
)

type BusinessType struct {
	gorm.Model
	ID   uint   `gorm:"primaryKey;AUTO_INCREMENT;column:id"`
	Code string `gorm:"column:code"`
	Name string `gorm:"column:name"`

	CreatedBy *string    `gorm:"column:created_by"`
	UpdatedBy *string    `gorm:"column:updated_by"`
	DeletedBy *string    `gorm:"column:deleted_by"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
}

func (BusinessType) TableName() string {
	return "business_type"
}

type BusinessTypeResponse struct {
	Status       int
	Message      string
	MessageLocal string
}

type BusinessTypeMapping struct {
	ID   int    `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`

	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

func GetBusinessType() (response BusinessTypeResponse, data []BusinessTypeMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	response = BusinessTypeResponse{
		Status:       400,
		Message:      cc.ErorrGeneralMessage,
		MessageLocal: cc.ErorrDBDefaultMessageLocal,
	}

	/* open connection */
	db, err := configs.InitDb()
	if err != nil {
		response = BusinessTypeResponse{
			Status:       400,
			Message:      cc.ErorrGeneralMessage,
			MessageLocal: err.Error(),
		}

		return response, data
	}
	/* end open connection */

	/* build query */
	var result []BusinessType

	query := db.Order("name asc").Find(&result)
	/* end query */

	/* error handling */
	if query.Error != nil { /*error query*/
		if query.Statement.Error != nil {
			response = BusinessTypeResponse{
				Status:       500,
				Message:      "Jenis Usaha Tidak Ditemukan",
				MessageLocal: query.Statement.Error.Error(),
			}

			log.Println("Model -- ERROR QUERY STATMENT BUSINESS TYPE --")
			log.Println("Model -- ERROR QUERY STATMENT BUSINESS TYPE : ", query.Statement.Error.Error())
		}

		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			response = BusinessTypeResponse{
				Status:       500,
				Message:      "Jenis Usaha Tidak Ditemukan",
				MessageLocal: cc.ErorrDBRecordNotFoundMessageLocal,
			}

			log.Println("Model -- DATA NOT FOUND BUSINESS TYPE --")
		}

		return response, data
	}

	if len(result) < 1 { /*data empty*/
		response = BusinessTypeResponse{
			Status:       404,
			Message:      "Jenis Usaha Tidak Ditemukan",
			MessageLocal: cc.ErorrDBRecordNotFoundMessageLocal,
		}

		log.Println("Model -- DATA EMPTY BUSINESS TYPE --")

		return response, data
	}
	/* end error handling */

	/* mapping data to struct */
	for _, v := range result {
		var allData = BusinessTypeMapping{
			ID:        int(v.ID),
			Code:      v.Code,
			Name:      v.Name,
			CreatedAt: v.CreatedAt,
			UpdatedAt: v.UpdatedAt,
			DeletedAt: v.DeletedAt,
		}

		data = append(data, allData) /*set result with data*/
	}
	/* end mapping data to struct */

	log.Println("Model -- RESPONSE MODEL BUSINESS TYPE --")
	log.Println(data)

	response = BusinessTypeResponse{
		Status:       200,
		Message:      cc.DBDataDitemukanMessage,
		MessageLocal: cc.DBDataValidMessageLocal,
	}

	return response, data
}

type BusinessTypeList struct {
	BusinessTypeCode int    `json:"id"`
	BusinessType     string `json:"name"`
}
type BusinessTypeLists []BusinessTypeList

type Login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type User struct {
	UserName string
}
