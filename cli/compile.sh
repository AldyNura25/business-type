#!/bin/bash
echo "starting build proto"

PATH=$PATH:$GOPATH/bin/ protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/v1/business_type/*.proto

echo "successfully generate"

# protoc \
#       -I. \
#       -I/usr/local/include \
#       -I${GOPATH}/src \
#       -I${GOPATH}/src/gitlab.com/business-type/proto \
#       --go_out=plugins=grpc:$GOPATH/src \
#       ${GOPATH}/src/gitlab.com/business-type/proto/v1/business_type/*.proto
