package main

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"

	config "gitlab.com/business-type/configs"
	util "gitlab.com/business-type/helpers/utils"
	model "gitlab.com/business-type/models/v1"
	pb "gitlab.com/business-type/proto/v1/business_type"
)

type server struct {
	pb.BusisnessTypesServer
}

func (s *server) BusinessTypeList(ctx context.Context, request *pb.BusinessTypeRequest) (*pb.BusinessTypeResponse, error) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "services", "/", "master", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	response := &pb.BusinessTypeResponse{}

	var responseResult model.BusinessTypeResponse
	var dataResult []model.BusinessTypeMapping
	responseResult, dataResult = model.GetBusinessType()

	log.Println("Service ** RESULT QUERY BUSINESS TYPE LIST **")
	log.Println(response)

	var resultData []*pb.DataBusisnessType
	for _, vData := range dataResult {
		dataRow := pb.DataBusisnessType{
			Id:   int64(vData.ID),
			Name: vData.Name,
		}

		resultData = append(resultData, &dataRow)
	}

	//response success
	response = &pb.BusinessTypeResponse{
		Status:  200,
		Desc:    responseResult.Message,
		Message: responseResult.MessageLocal,
		Data:    resultData,
	}

	//PROCESS TO LOGGING CLOUD
	{
		requestData := map[string]interface{}{}

		dataRequest, err := json.Marshal(requestData)
		if err != nil {
			log.Println("Service ** ERROR LOGGING CLOUD : ", err)

			response = &pb.BusinessTypeResponse{
				Status:  200,
				Desc:    responseResult.Message,
				Message: responseResult.MessageLocal,
			}

			return response, nil
		}

		log.Println("Service ** DATA REQUEST**")
		log.Println(string(dataRequest))

		//DATA RESPONSE
		strStatusCode, _ := util.IntString(200)
		responseData := map[string]interface{}{
			"statusCode":   strStatusCode,
			"responseData": response,
		}

		dataResponse, err := json.Marshal(responseData)
		if err != nil {
			log.Println("Service ** ERROR LOGGING CLOUD : ", err)

			response = &pb.BusinessTypeResponse{
				Status:  200,
				Desc:    responseResult.Message,
				Message: responseResult.MessageLocal,
			}

			return response, nil
		}

		log.Println("Service ** DATA RESPONSE**")
		log.Println(string(dataResponse))

		runtime.GOMAXPROCS(1)
		var wg sync.WaitGroup

		dLogStatus := make(chan int, 2)
		dLogMessage := make(chan string, 2)

		wg.Add(1)
		go func() {
			dataLog := config.V1LoggingCloudPubSub{
				Status:       "200",
				Endpoint:     "v/1/master/business-type",
				UserId:       "",
				ActionDate:   time.Now().Format("2006-01-02 15:04:05.000"),
				Description:  "master-logging",
				DataRequest:  string(dataRequest),
				DataResponse: string(dataResponse),
			}

			logData, _ := json.Marshal(dataLog)
			logJson := string(logData)

			logStatus, logMessage := util.LoggingCloudPubSubV1(&wg, logJson, "master-cloud-log")
			dLogStatus <- logStatus
			dLogMessage <- logMessage

			// log.Println("Service ** RESULT SERVICE PROCESS LOGGING CLOUD **")
			// log.Println(logStatus)
			// log.Println(logMessage)
		}()

		var statusLog = <-dLogStatus
		var messageLog = <-dLogMessage
		log.Println("Service ** RESULT SERVICE PROCESS LOGGING CLOUD **")
		log.Println(statusLog)
		log.Println(messageLog)
	}

	return response, nil
}

func main() {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "service", "/", "v1", "/", "business_type", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	port := os.Getenv("PORT_SERVICE_MASTER")
	log.Println("--- START SERVICE MASTER V1 --- ", "PORT", port)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Println("Failed to listen : ", err)
	}

	// var opts []grpc.ServerOption

	// grpcServer := grpc.NewServer(opts...)

	grpcServer := grpc.NewServer(
		grpc.MaxRecvMsgSize(1024*1024*20),
		grpc.MaxSendMsgSize(1024*1024*20),
	)

	pb.RegisterBusisnessTypesServer(grpcServer, &server{})

	if err := grpcServer.Serve(lis); err != nil {
		log.Println("Failed to server : ", err)
	}

}
